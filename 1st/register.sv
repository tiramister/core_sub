module register (ridx1, rdata1, ridx2, rdata2, widx, wdata, wenable, clk);
   parameter AWIDTH = 5;
   parameter DWIDTH = 32;
   parameter SIZE = 32;
   
   input [AWIDTH-1:0] ridx1, ridx2, widx;
   // read / write index
   output logic [DWIDTH-1:0] rdata1, rdata2;
   // read data
   input [DWIDTH-1:0] wdata;
   // written data
   input              wenable;
   // whether to overwrite data[widx] with wdata
   input              clk;

   logic signed [DWIDTH-1:0] data [SIZE];

   always_ff @(posedge clk) begin
      rdata1 <= data[ridx1];
      rdata2 <= data[ridx2];

      if (wenable) begin
         data[widx] <= wdata;
      end
   end
endmodule // register
