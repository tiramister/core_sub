module alu(in1, in2, out, opcode);
   parameter VWIDTH = 32;
   parameter OWIDTH = 3;
   
   input signed [VWIDTH-1:0] in1;
   input signed [VWIDTH-1:0] in2;
   output logic signed [VWIDTH-1:0] out;
   input         [OWIDTH-1:0] opcode;

   always_comb begin
      case (opcode)
        3'b000: out = in1 + in2;
        3'b001: out = in1 - in2;
        3'b010: out = in1 * in2;
        3'b011: out = in1 / in2;
        3'b100: out = in1 % in2;
        default: out = '{VWIDTH{1'bx}}; // undefined
      endcase
   end
endmodule // alu
