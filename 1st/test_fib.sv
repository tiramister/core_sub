`timescale 1ns / 1ps

module test_fib ();
   parameter VWIDTH = 32;
   parameter OWIDTH = 3;
   parameter AWIDTH = 5;
   parameter LOOP = 1000;

   logic unsigned [AWIDTH-1:0] ridx1, ridx2, widx, cidx;
   wire signed [VWIDTH-1:0]    rdata1, rdata2;
   logic signed [VWIDTH-1:0]   wdata;
   logic                       wenable, clk;
   
   register rgst(.ridx1(ridx1), .rdata1(rdata1),
                 .ridx2(ridx2), .rdata2(rdata2),
                 .widx(widx), .wdata(wdata),
                 .wenable(wenable), .clk(clk));

   logic [VWIDTH-1:0]          result;
   logic [OWIDTH-1:0]          opcode;

   alu alu(.in1(rdata1), .in2(rdata2), .out(result), .opcode(opcode));

   integer cnt = 0;
   
   // initialize clock and so on
   initial begin
      clk = 0;
   end

   always begin
      #5;
      clk = ~clk;
   end

   always_ff @(posedge clk) begin
      if (cnt == 0) begin
         // initialize
         wenable = 0;
         cidx = 1;
         opcode = 3'b000; // add
      end else if (cnt <= 2) begin
         // data[i] <= i (i = 1, 2)
         wdata <= cnt;
         widx <= cidx;
         wenable <= 1;
         cidx <= cidx + 1;
      end else if (cnt == LOOP) begin
         $finish;
      end else if (cnt % 4 == 0) begin
         // let alu calculate data[cidx - 1] + data[cidx - 2]
         wenable <= 0;
         ridx1 <= cidx - 2;
         ridx2 <= cidx - 1;
      end else if (cnt % 2 == 0) begin
         // data[cidx] <= result (= data[cidx-1] + data[cidx-2])
         wdata <= result;
         widx <= cidx;
         wenable <= 1;
         cidx <= cidx + 1;
      end
      cnt <= cnt + 1;
   end
endmodule // test_fib
